<!DOCTYPE html>
<html lang="en">
<head>
    <title>Aplikasi Database</title>
</head>
<body>

    <h1>Aplikasi Database</h1>

    <form action="CreateDatabase" method="post">
        <input type="text" name="databaseName" placeholder="Nama database">
        <input type="submit" value="Buat database">
    </form>

    <form action="SaveData" method="post">
        <input type="text" name="databaseName" placeholder="Nama database">
        <input type="text" name="tableName" placeholder="Nama tabel">
        <input type="text" name="id" placeholder="ID">
        <input type="text" name="nama" placeholder="Nama">
        <input type="text" name="harga_beli" placeholder="Harga beli">
        <input type="text" name="harga_jual" placeholder="Harga jual">
        <input type="submit" value="Simpan data">
    </form>

    <form action="ReadData" method="get">
        <input type="text" name="databaseName" placeholder="Nama database">
        <input type="text" name="tableName" placeholder="Nama tabel">
        <input type="text" name="criteria" placeholder="Kriteria pencarian">
        <input type="submit" value="Baca data">
    </form>

    <h2>Buat tabel database</h2>

    <textarea name="sql" rows="10" cols="50"></textarea>
    <input type="submit" value="Execute">
</body>
</html>
