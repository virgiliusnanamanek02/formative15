package co.id.nexsoft.env;

import java.io.InputStream;
import java.util.Properties;

public class DatabaseConfig {
    private static final String CONFIG_FILE = "database.properties";

    private static Properties properties;

    static {
        properties = new Properties();
        try (InputStream input = DatabaseConfig.class.getClassLoader().getResourceAsStream(CONFIG_FILE)) {
            properties.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getDatabaseUrl() {
        return properties.getProperty("database.url");
    }

    public static String getDatabaseUsername() {
        return properties.getProperty("database.username");
    }

    public static String getDatabasePassword() {
        return properties.getProperty("database.password");
    }

    public static int getDatabasePort() {
        return Integer.parseInt(properties.getProperty("database.port"));
    }
}
