package co.id.nexsoft;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;

import co.id.nexsoft.env.DatabaseConfig;

import java.sql.SQLException;

@WebServlet("/CreateDatabase")
public class CreateDatabaseServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String sql = request.getParameter("sql");

        Connection connection = null;
        try {
            // Load the JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Connect to the database
            connection = DriverManager.getConnection(DatabaseConfig.getDatabaseUrl(), DatabaseConfig.getDatabaseUsername(), DatabaseConfig.getDatabasePassword());

            // Create the database
            connection.createStatement().execute(sql);

            // Close the connection
            connection.close();

            // Redirect to the home page
            response.sendRedirect("index.jsp");
        } catch (ClassNotFoundException | SQLException e) {
            throw new ServletException(e);
        }
    }
}

